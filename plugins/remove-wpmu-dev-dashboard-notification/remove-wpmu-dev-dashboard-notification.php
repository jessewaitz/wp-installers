<?php
/*
Plugin Name: Remove WPMU DEV Dashboard Notification
Plugin URI: http://premium.wpmudev.org
Description: This will remove the WPMU DEV Dashboard message that asks you to install the plugin. Please note that not using our plugin can remove protection from your site, please rewview the following link and then if you're sure you still don't want to use it, just use this plugin: <a href="http://premium.wpmudev.org/wpmu-dev/update-notifications-plugin-information/">http://premium.wpmudev.org/wpmu-dev/update-notifications-plugin-information/</a>
Version: 1
Author: Timothy Bowers, tosco
Author URI: http://premium.wpmudev.org
*/

//We're simply creating an empty class so that they exist when our plugins look for them.
if ( !class_exists('WPMUDEV_Dashboard_Notice') ) {
	class WPMUDEV_Dashboard_Notice {}
}
 
//This is legacy for older plugins that still use our previous method of adding the dashboard notice.
if ( function_exists( 'wdp_un_check' ) ) {
  remove_action( 'admin_notices', 'wdp_un_check', 5 );
  remove_action( 'network_admin_notices', 'wdp_un_check', 5 );
}

if ( ! function_exists( 'remove_anonymous_object_filter' ) )
{
    /**
     * Remove an anonymous object filter. Code credits to toscho
     *
     * @param  string $tag    Hook name.
     * @param  string $class  Class name
     * @param  string $method Method name
     * @return void
     */
    function remove_anonymous_object_filter( $tag, $class, $method )
    {
        $filters = $GLOBALS['wp_filter'][ $tag ];

        if ( empty ( $filters ) )
        {
            return;
        }

        foreach ( $filters as $priority => $filter )
        {
            foreach ( $filter as $identifier => $function )
            {
                if ( is_array( $function)
                    and is_a( $function['function'][0], $class )
                    and $method === $function['function'][1]
                )
                {
                    remove_filter(
                        $tag,
                        array ( $function['function'][0], $method ),
                        $priority
                    );
                }
            }
        }
    }
}
add_action( 'init', 'kill_anonymous_example', 0 );
function kill_anonymous_example()
{
    remove_anonymous_object_filter(
        'init',
        'WPMUDEV_Dashboard_Notice3',
        'init'
    );
}