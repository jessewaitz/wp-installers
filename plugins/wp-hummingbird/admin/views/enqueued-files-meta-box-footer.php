<div class="buttons alignleft">
	<button type="submit" class="button button-grey" name="clear-cache"><?php esc_attr_e( 'Re-Check Files', 'wphb' ); ?></button>
</div>
<div class="buttons alignright">
	<button class="button button-grey wphb-discard" <?php disabled( true ); ?>><?php esc_attr_e( 'Discard Changes', 'wphb' ); ?></button>
	<button type="submit" class="button button-app" name="submit"><?php esc_attr_e( 'Save Changes', 'wphb' ); ?></button>
</div>
<div class="clear"></div>