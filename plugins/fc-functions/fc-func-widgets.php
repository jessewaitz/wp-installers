<?php
add_action('customize_controls_print_styles',function() { 
	echo file_get_contents(PLUGURL.'inc/iconset.htm'); 
} );

class CTA_Widget extends WP_Widget {	
	function __construct() {
		parent::__construct( 'fc_cta_widget', __('Call To Action', 'fc'), array( 'description' => __( 'Create call to action widgets to appear in the sidebar or home page.', 'fc' ), ) );
	}

	// Creating widget front-end
	public function widget( $args, $instance ) { global $i; $i++; //echo "<pre>"; print_r($i); echo "</pre>";
    wp_enqueue_style( 'wp-color-picker' );        
    wp_enqueue_script( 'wp-color-picker' ); 
		$title = apply_filters( 'widget_title', $instance['title'] );
		$title_size = ($instance['title_size']) ? $instance['title_size']: '20px';
		$title_weight = ($instance['title_weight']) ? $instance['title_weight']: 'bold';
		$sub_title = $instance['sub_title'];
		$sub_title_size = ($instance['sub_title_size']) ? $instance['sub_title_size']: '16px';
		$sub_title_weight = ($instance['sub_title_weight']) ? $instance['sub_title_weight']: 'normal';
		$button_url = $instance['button_url'];
		$font_link_color = ($instance['font_link_color']) ? $instance['font_link_color']: '#fff';
		$font_link_hover = ($instance['font_link_hover']) ? $instance['font_link_hover']: 'red';
		$bg_color = ($instance['bg_color']) ? $instance['bg_color']: 'transparent';
		$bg_hover = ($instance['bg_hover']) ? $instance['bg_hover']: '#fff';
		$border_color = ($instance['border_color']) ? $instance['border_color']: '#000';
		$box_padding = ($instance['box_padding']) ? $instance['box_padding']: '10px';
		$font_alignment = ($instance['font_alignment']) ? $instance['font_alignment']: 'center';
		$border_width = ($instance['border_width']) ? $instance['border_width']: '1px';
		$border_radius = ($instance['border_radius']) ? $instance['border_radius']: '5px';
		$icon = $instance['icon'];
		$icon_size = ($instance['icon_size']) ? $instance['icon_size']: '50px';
		$icon_position = ($instance['icon_position']) ? $instance['icon_position']: 'top';
		
		// before and after widget arguments are defined by themes
		echo $args['before_widget']; ?>
		<style type="text/css" media="screen" >
			#<?php echo $this->id; ?> .fc-cta-button { background: <?php echo $bg_color; ?>; border: <?php echo $border_color; ?> solid <?php echo $border_width; ?>; border-radius: <?php echo $border_radius; ?>; padding: 0; text-align: <?php echo $font_alignment; ?>; }
			#<?php echo $this->id; ?> .fc-cta-button:hover { background: <?php echo $bg_hover; ?>; }
			#<?php echo $this->id; ?> .fc-cta-button a { color: <?php echo $font_link_color; ?>!important; border: 0!important; display: block; padding: <?php echo $box_padding; ?>; }
			#<?php echo $this->id; ?> .fc-cta-button a:hover { color: <?php echo $font_link_hover; ?>!important; text-decoration: none!important; }
			#<?php echo $this->id; ?> .fc-cta-button .fc-cta-title { font-size: <?php echo $title_size; ?>; font-weight: <?php echo $title_weight; ?>; }
			#<?php echo $this->id; ?> .fc-cta-button .fc-cta-sub-title { font-size: <?php echo $sub_title_size; ?>; font-weight: <?php echo $sub_title_weight; ?>; }
			#<?php echo $this->id; ?> .fc-cta-icon { font-size: <?php echo $icon_size; ?>; width: <?php echo $icon_size; ?>; height: <?php echo $icon_size; ?>; display: inline-block; }
			<?php if ($icon_position == 'top') { ?>
			#<?php echo $this->id; ?> .fc-cta-icon_wrap, .fc-cta-text_wrap { display: block; }
			<?php } if ($icon_position == 'left') { ?>
			#<?php echo $this->id; ?> .fc-cta-icon_wrap { display: block; float: left; }
			#<?php echo $this->id; ?> .fc-cta-text_wrap { display: block; float: right; }
			<?php } if ($icon_position == 'right') { ?>
			#<?php echo $this->id; ?> .fc-cta-icon_wrap { display: block; float: right; }
			#<?php echo $this->id; ?> .fc-cta-text_wrap { display: block; float: left; }
			<?php } ?>
		</style>
		<div class="fc-cta-button">
			<?php if ( $button_url ) { ?><a href="<?php echo $button_url; ?>"><?php } ?>
				<div class="fc-cta-icon_wrap">
					<?php if ( $icon ) { echo '<i class="fc-cta-icon '.$icon.'"></i>'; } ?>
				</div>
				<div class="fc-cta-text_wrap">
					<?php if ( $title ) { echo '<div class="fc-cta-title">'.$title.'</div>'; } ?>
					<?php if ( $sub_title ) { echo '<div class="fc-cta-sub-title">'.$sub_title.'</div>'; } ?>
				</div>
			<?php if ( $button_url ) { ?></a><?php } ?>
			<div class="clear"></div>
		</div>
	<?php echo $args['after_widget'];
	}
	
	// Widget Backend 
	public function form( $instance ) { global $i; $i++; //echo "<pre>"; print_r($instance); echo "</pre>";
		$title = (isset($instance['title'])) ? $instance[ 'title' ] : '';
		$title_size = (isset($instance['title_size'])) ? $instance['title_size'] : '';
		$title_weight = (isset($instance['title_weight'])) ? $instance['title_weight'] : '';
		$sub_title = (isset($instance['sub_title'])) ? $instance['sub_title'] : '';
		$sub_title_size = (isset($instance['sub_title_size'])) ? $instance['sub_title_size'] : '';
		$sub_title_weight = (isset($instance['sub_title_weight'])) ? $instance['sub_title_weight'] : '';
		$button_url = (isset($instance['button_url'])) ? $instance['button_url'] : '';
		$font_link_color = (isset($instance['font_link_color'])) ? $instance['font_link_color'] : '';
		$font_link_hover = (isset($instance['font_link_hover'])) ? $instance['font_link_hover'] : '';
		$bg_color = (isset($instance['bg_color'])) ? $instance['bg_color'] : '';
		$bg_hover = (isset($instance['bg_hover'])) ? $instance['bg_hover'] : '';
		$box_padding = (isset($instance['box_padding'])) ? $instance['box_padding'] : '';
		$font_alignment = (isset($instance['font_alignment'])) ? $instance['font_alignment'] : '';
		$border_color = (isset($instance['border_color'])) ? $instance['border_color'] : '';
		$border_width = (isset($instance['border_width'])) ? $instance['border_width'] : '';
		$border_radius = (isset($instance['border_radius'])) ? $instance['border_radius'] : '';
		$icon = (isset($instance['icon'])) ? $instance['icon'] : '';
		$icon_size = (isset($instance['icon_size'])) ? $instance['icon_size'] : '';
		$icon_position = (isset($instance['icon_position'])) ? $instance['icon_position'] : '';
		// Widget admin form
		if ($i>1) { ?>
		<style type="text/css" media="screen">
			.fc-cta-half { float: left; width: 50%; }
			.fc-cta-show-hide { float: right; }
			.fc-cta-section-head { border-bottom: #ccc solid 1px; margin-bottom: 0; }
			.fc-cta-section-wrapper { display: none; background: #f2f2f2; padding: 0 10px 10px 10px; margin-bottom: 10px; border-radius: 0px 0px 10px 10px; }
			.fc-cta-section-wrapper p { margin-top: 0; }
			.widget-content { margin-bottom: 20px; }
		</style>
	  <script type='text/javascript'>
			jQuery(document).ready(function($) {
				$('.my-color-picker').wpColorPicker();
				$('.widget-content .wp-color-picker').wpColorPicker({ change: function(event,ui){ $('.icon-picker input').trigger('change'); } });
				$('.icon-picker').qlIconPicker();
				$('.launch-icons').click(function(){ $(".fa-set.icon-set").show(); });
				$('.fc-cta-show-hide').click( function(){ //console.log($(this).parent().next('.fc-cta-section-wrapper'));
					var target = $(this).parent().next('.fc-cta-section-wrapper');
					if (target.css('display') === 'none') { target.slideDown(); $(this).text('Hide'); }
					else if (target.css('display') === 'block') { target.slideUp(); $(this).text('Show'); }
				});
			});
	  </script>
	  <?php } ?>
	  <h3 class="fc-cta-section-head">Button Content <a href="#none" class="fc-cta-show-hide">Hide</a></h3>
		<div class="fc-cta-section-wrapper" style="display: block;">
			<div>
				<div>
					<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'fc' ); ?></label> 
					<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
				</div>
				<div class="fc-cta-half">
					<input class="widefat" id="<?php echo $this->get_field_id( 'title_size' ); ?>" name="<?php echo $this->get_field_name( 'title_size' ); ?>" type="text" value="<?php echo esc_attr( $title_size ); ?>" placeholder="<?php _e( 'Size (ie. 20px)' ); ?>" style="margin-top: 2px;" />
				</div>
				<div class="fc-cta-half">
					<select name="<?php echo $this->get_field_name( 'title_weight' ); ?>" id="<?php echo $this->get_field_id( 'title_weight' ); ?>">
						<option <?php if ($title_weight == "") { echo 'selected="selected"'; } ?> value=""><?php _e( 'Font Weight:' ); ?></option>
						<option <?php if ($title_weight == "normal") { echo 'selected="selected"'; } ?> value="normal">Normal</option>
						<option <?php if ($title_weight == "bold") { echo 'selected="selected"'; } ?> value="bold">Bold</option>
					</select>
				</div><div class="clear"></div>
			</div><br/>
			<div>
				<label for="<?php echo $this->get_field_id( 'sub_title' ); ?>"><?php _e( 'Sub Title:' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'sub_title' ); ?>" name="<?php echo $this->get_field_name( 'sub_title' ); ?>" type="text" value="<?php echo esc_attr( $sub_title ); ?>" />
				<div class="fc-cta-half">
					<input class="widefat" id="<?php echo $this->get_field_id( 'sub_title_size' ); ?>" name="<?php echo $this->get_field_name( 'sub_title_size' ); ?>" type="text" value="<?php echo esc_attr( $sub_title_size ); ?>" placeholder="<?php _e( 'Size (ie. 20px)' ); ?>" style="margin-top: 2px;" />
				</div>
				<div class="fc-cta-half">
					<select name="<?php echo $this->get_field_name( 'sub_title_weight' ); ?>" id="<?php echo $this->get_field_id( 'sub_title_weight' ); ?>">
						<option <?php if ($sub_title_weight == "") { echo 'selected="selected"'; } ?> value=""><?php _e( 'Font Weight:' ); ?></option>
						<option <?php if ($sub_title_weight == "normal") { echo 'selected="selected"'; } ?> value="normal">Normal</option>
						<option <?php if ($sub_title_weight == "bold") { echo 'selected="selected"'; } ?> value="bold">Bold</option>
					</select>
				</div><div class="clear"></div>
			</div>
		</div>
	  <h3 class="fc-cta-section-head">Link Settings<a href="#none" class="fc-cta-show-hide">Hide</a></h3>
		<div class="fc-cta-section-wrapper">
			<div>
				<label for="<?php echo $this->get_field_id( 'button_url' ); ?>"><?php _e( 'Button Link URL:' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'button_url' ); ?>" name="<?php echo $this->get_field_name( 'button_url' ); ?>" type="text" value="<?php echo esc_attr( $button_url ); ?>" placeholder="http://domain.com" />
			</div><br/>
		  <div>
		    <label for="<?php echo $this->get_field_id( 'font_link_color' ); ?>"><?php _e( 'Text Link Color', 'fc' ); ?></label><br/>
		    <input class="my-color-picker" id="<?php echo $this->get_field_id( 'font_link_color' ); ?>" name="<?php echo $this->get_field_name( 'font_link_color' ); ?>" type="text" value="<?php echo esc_attr( $font_link_color ); ?>" />                            
		  </div>
		  <div>
		    <label for="<?php echo $this->get_field_id( 'font_link_hover' ); ?>"><?php _e( 'Text Hover Color', 'fc' ); ?></label><br/>
		    <input class="my-color-picker" id="<?php echo $this->get_field_id( 'font_link_hover' ); ?>" name="<?php echo $this->get_field_name( 'font_link_hover' ); ?>" type="text" value="<?php echo esc_attr( $font_link_hover ); ?>" />                            
		  </div>
		</div>
		<h3 class="fc-cta-section-head">Box Settings <a href="#none" class="fc-cta-show-hide">Show</a></h3>
		<div class="fc-cta-section-wrapper">
		  <div>
		    <label for="<?php echo $this->get_field_id( 'bg_color' ); ?>"><?php _e( 'BG Color', 'fc' ); ?></label><br/>
		    <input class="my-color-picker" id="<?php echo $this->get_field_id( 'bg_color' ); ?>" name="<?php echo $this->get_field_name( 'bg_color' ); ?>" type="text" value="<?php echo esc_attr( $bg_color ); ?>" />                            
		  </div>
		  <div>
		    <label for="<?php echo $this->get_field_id( 'bg_hover' ); ?>"><?php _e( 'BG Hover Color', 'fc' ); ?></label><br/>
		    <input class="my-color-picker" id="<?php echo $this->get_field_id( 'bg_hover' ); ?>" name="<?php echo $this->get_field_name( 'bg_hover' ); ?>" type="text" value="<?php echo esc_attr( $bg_hover ); ?>" />                            
		  </div>
		  <div>
		    <label for="<?php echo $this->get_field_id( 'border_color' ); ?>"><?php _e( 'Border Color', 'fc' ); ?></label><br/>
		    <input class="my-color-picker" id="<?php echo $this->get_field_id( 'border_color' ); ?>" name="<?php echo $this->get_field_name( 'border_color' ); ?>" type="text" value="<?php echo esc_attr( $border_color ); ?>" />                            
		  </div><br/>
			<div class="fc-cta-half">
				<label for="<?php echo $this->get_field_id( 'box_padding' ); ?>"><?php _e( 'Box Padding:' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'box_padding' ); ?>" name="<?php echo $this->get_field_name( 'box_padding' ); ?>" type="text" value="<?php echo esc_attr( $box_padding ); ?>" placeholder="ie. 10px" />
			</div>
			<div class="fc-cta-half">
				<label for="<?php echo $this->get_field_id( 'font_alignment' ); ?>"><?php _e( 'Text Alignment:' ); ?></label> 
				<select name="<?php echo $this->get_field_name( 'font_alignment' ); ?>" id="<?php echo $this->get_field_id( 'font_alignment' ); ?>">
					<option <?php if ($font_alignment == "") { echo 'selected="selected"'; } ?> value="">Select</option>
					<option <?php if ($font_alignment == "center") { echo 'selected="selected"'; } ?> value="center">Center align</option>
					<option <?php if ($font_alignment == "left") { echo 'selected="selected"'; } ?> value="left">Left align</option>
					<option <?php if ($font_alignment == "right") { echo 'selected="selected"'; } ?> value="right">Right align</option>
				</select>
			</div>
			<div class="fc-cta-half">
				<label for="<?php echo $this->get_field_id( 'border_width' ); ?>"><?php _e( 'Border Width:' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'border_width' ); ?>" name="<?php echo $this->get_field_name( 'border_width' ); ?>" type="text" value="<?php echo esc_attr( $border_width ); ?>" placeholder="ie. 1px" />
			</div>
			<div class="fc-cta-half">
				<label for="<?php echo $this->get_field_id( 'border_radius' ); ?>"><?php _e( 'Border Radius:' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'border_radius' ); ?>" name="<?php echo $this->get_field_name( 'border_radius' ); ?>" type="text" value="<?php echo esc_attr( $border_radius ); ?>" placeholder="ie. 10px" />
			</div>
			<div class="clear"></div>
		</div>
	  <h3 class="fc-cta-section-head">Icon Settings <a href="#none" class="fc-cta-show-hide">Show</a></h3>
		<div class="fc-cta-section-wrapper">
			<div class="fc-cta-half icon-picker" data-pickerid="fa" data-iconsets='{"fa":"Select Icon"}'>
				<input type="hidden" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>" value="<?php echo esc_attr( $icon ); ?>" />
			</div>
			<div class="fc-cta-half"><br/>
				<label for="<?php echo $this->get_field_id( 'icon_size' ); ?>"><?php _e( 'Size of Icon:' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'icon_size' ); ?>" name="<?php echo $this->get_field_name( 'icon_size' ); ?>" type="text" value="<?php echo esc_attr( $icon_size ); ?>" placeholder="ie. 50px" />
				<br/><br/>
				<label for="<?php echo $this->get_field_id( 'icon_position' ); ?>"><?php _e( 'Position of Icon:' ); ?></label> 
				<select name="<?php echo $this->get_field_name( 'icon_position' ); ?>" id="<?php echo $this->get_field_id( 'icon_position' ); ?>">
					<option <?php if ($icon_position == "") { echo 'selected="selected"'; } ?> value="">Select</option>
					<option <?php if ($icon_position == "top") { echo 'selected="selected"'; } ?> value="top">Above text</option>
					<option <?php if ($icon_position == "left") { echo 'selected="selected"'; } ?> value="left">Left of text</option>
					<option <?php if ($icon_position == "right") { echo 'selected="selected"'; } ?> value="right">Right of text</option>
				</select>
			</div>
			<div class="clear"></div>
		</div>
	<?php }
	
// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['title_size'] = ( ! empty( $new_instance['title_size'] ) ) ? strip_tags( $new_instance['title_size'] ) : '';
		$instance['title_weight'] = ( ! empty( $new_instance['title_weight'] ) ) ? strip_tags( $new_instance['title_weight'] ) : '';
		$instance['sub_title'] = ( ! empty( $new_instance['sub_title'] ) ) ? strip_tags( $new_instance['sub_title'] ) : '';
		$instance['sub_title_size'] = ( ! empty( $new_instance['sub_title_size'] ) ) ? strip_tags( $new_instance['sub_title_size'] ) : '';
		$instance['sub_title_weight'] = ( ! empty( $new_instance['sub_title_weight'] ) ) ? strip_tags( $new_instance['sub_title_weight'] ) : '';
		$instance['button_url'] = ( ! empty( $new_instance['button_url'] ) ) ? strip_tags( $new_instance['button_url'] ) : '';
		$instance['font_link_color'] = ( ! empty( $new_instance['font_link_color'] ) ) ? strip_tags( $new_instance['font_link_color'] ) : '';
		$instance['font_link_hover'] = ( ! empty( $new_instance['font_link_hover'] ) ) ? strip_tags( $new_instance['font_link_hover'] ) : '';
		$instance['bg_color'] = ( ! empty( $new_instance['bg_color'] ) ) ? strip_tags( $new_instance['bg_color'] ) : '';
		$instance['bg_hover'] = ( ! empty( $new_instance['bg_hover'] ) ) ? strip_tags( $new_instance['bg_hover'] ) : '';
		$instance['box_padding'] = ( ! empty( $new_instance['box_padding'] ) ) ? strip_tags( $new_instance['box_padding'] ) : '';
		$instance['font_alignment'] = ( ! empty( $new_instance['font_alignment'] ) ) ? strip_tags( $new_instance['font_alignment'] ) : '';
		$instance['border_color'] = ( ! empty( $new_instance['border_color'] ) ) ? strip_tags( $new_instance['border_color'] ) : '';
		$instance['border_width'] = ( ! empty( $new_instance['border_width'] ) ) ? strip_tags( $new_instance['border_width'] ) : '';
		$instance['border_radius'] = ( ! empty( $new_instance['border_radius'] ) ) ? strip_tags( $new_instance['border_radius'] ) : '';
		$instance['icon'] = ( ! empty( $new_instance['icon'] ) ) ? strip_tags( $new_instance['icon'] ) : '';
		$instance['icon_size'] = ( ! empty( $new_instance['icon_size'] ) ) ? strip_tags( $new_instance['icon_size'] ) : '';
		$instance['icon_position'] = ( ! empty( $new_instance['icon_position'] ) ) ? strip_tags( $new_instance['icon_position'] ) : '';
		return $instance;
	}
} // Class fc_cta-widget ends here

// Register and load the widget
function fc_cta_load_widget() { register_widget( 'CTA_Widget' ); }
add_action( 'widgets_init', 'fc_cta_load_widget' );
?>