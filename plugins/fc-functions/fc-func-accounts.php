<?php

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% REDIRECT USERS AFTER THE LOGIN BASED ON USER ROLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function fc_login_redirect( $redirect_to, $request, $user  ) { return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url()."/my-profile/"; }
add_filter( 'login_redirect', 'fc_login_redirect', 10, 3 );

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% FIXES THE ISSUE WITH FAILED LOGOUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function smart_logout() {
	if (!is_user_logged_in()) {
		wp_safe_redirect( home_url()."/my-profile/" ); exit;
	} else {
		check_admin_referer('log-out');
		wp_logout();
		$smart_redirect_to = !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '/';
		wp_safe_redirect( $smart_redirect_to );
		exit();
	}
}
add_action ( 'login_form_logout' , 'smart_logout' );

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% FUNCTIONS TO REDIRECT TO MY ACCOUNT AFTER FAILED LOGIN OR PASSWORD RESET %%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
//add_action('password_reset', 'wpse_lost_password_redirect');
function wpse_lost_password_redirect() {
	wp_safe_redirect( home_url()."/my-profile/?reset2=true" ); exit;
}

//add_action( 'wp_login_failed', 'my_front_end_login_fail', 20 );
function my_front_end_login_fail( $username ) {
	$referrer = $_SERVER['HTTP_REFERER'];
	if ($_SERVER['HTTP_REFERER']!=home_url()."/my-profile/") {
		wp_safe_redirect( home_url()."/my-profile/" ); exit;
	} else {
		if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
			$pos1 = strpos($referrer, '?login=failed');
			$pos2 = strpos($referrer, '?');
			if($pos1 === false && $pos2 === false) {
				wp_redirect( $referrer . '?login=failed' );
			} elseif($pos1 === false && $pos2 == true) {
				wp_redirect( $referrer . '&login=failed' );
			} else { 
				wp_redirect( $referrer );
			}
			exit;
		}
	}
}

//add_filter( 'authenticate', 'custom_authenticate_username_password', 30, 3);
function custom_authenticate_username_password( $user, $username, $password ) {
    if ( is_a($user, 'WP_User') ) { return $user; }

    if ( empty($username) || empty($password) ) {
      $error = new WP_Error();
      $user  = new WP_Error('authentication_failed', __('<strong>ERROR</strong>: Invalid username or incorrect password.'));
      return $error;
    }
}

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% ALLOW THE LOGIN FORM TO USE EMAIL ADDRESS AS USERNAME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function email_address_login($username) {
	$user = get_user_by_email($username);
	if(!empty($user->user_login))
	$username = $user->user_login;
	return $username;
}
add_action('wp_authenticate','email_address_login'); ?>