<?php
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% SETUP SITE OPTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

// CHANGE THE DEFAULT CUSTOMIZER APPEARANCE
function fc_edit_customizer( $wp_customize ) {
	$wp_customize->get_section( 'title_tagline' )->title = __( 'Site Information', 'fc' );
	$wp_customize->get_panel( 'nav_menus' )->priority = 35;
	
	$wp_customize->get_control( 'blogdescription' )->title = __( 'Sub Title', 'fc' );
	$wp_customize->get_control( 'site_icon' )->section = 'fc_theme_design_layout';
	$wp_customize->get_control( 'site_icon' )->priority = 50;
	$wp_customize->get_control( 'custom_logo' )->section = 'fc_theme_design_layout';
	$wp_customize->get_control( 'custom_logo' )->priority = 40;
	
	$wp_customize->remove_section( 'static_front_page' );
  
	$widgets_panel = $wp_customize->get_panel( 'widgets' );
  if ( $widgets_panel ) {
    $widgets_panel->title = __( 'Sidebars', 'fc' );
    $widgets_panel->priority = 30;
  }
  //if ( $wp_customize->is_preview() && ! is_admin() ) { add_action( 'wp_footer', 'fc_customize_preview', 21); }
	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
}
add_action( 'customize_register' , 'fc_edit_customizer', 100 );

// INITIALIZE THE THE FOOTER SCRIPTS FOR THE WYSIWYG EDITORS
function fc_admin_init() { if (!did_action('admin_print_footer_scripts')) { do_action('admin_print_footer_scripts'); } }
add_action( 'customize_controls_print_scripts', 'fc_admin_init', 20 );

// CREATE CUSTOM CUSTOMIZER CONTROLS
function fc_theme_options( $wp_customize ) {
	// Require Text Editors
	require_once dirname(__FILE__).'/customizer-controls/text/text-editor-custom-control.php';
	require_once dirname(__FILE__).'/customizer-controls/text/text-editor-only-custom-control.php';
	
	// Create Sections
	$wp_customize->add_panel( 'fc_theme_footer', array(
    'title' => __( 'Footer', 'fc' ),													'priority' => 30,		'capability' => 'edit_theme_options',
	) );
	$wp_customize->add_panel( 'fc_theme_cta', array(
    'title' => __( 'Call To Action', 'fc' ),									'priority' => 30,		'capability' => 'edit_theme_options',
	) );
	$wp_customize->add_section( 'fc_theme_cta_options', array(
    'title' => __( 'Call To Action Options', 'fc' ),					'priority' => 1,		'capability' => 'edit_theme_options', 'panel' => 'fc_theme_cta',
	) );
	$wp_customize->add_section( 'fc_theme_footer_copy', array(
    'title' => __( 'Copyright Message', 'fc' ),								'priority' => 30,		'capability' => 'edit_theme_options', 'panel' => 'fc_theme_footer',
	) );
	$wp_customize->add_section( 'fc_theme_design_layout', array(
	 'title' => __( 'Design & Layout', 'fc' ),									'priority' => 25,		'capability' => 'edit_theme_options',
	) );
	$wp_customize->add_section( 'fc_theme_tweaks', array(
	 'title' => __( 'Tweaks & Settings', 'fc' ),								'priority' => 45,		'capability' => 'edit_theme_options',
	) );
	$wp_customize->add_section( 'fc_theme_custom_code', array(
	 'title' => __( 'Custom Code -- Google Analytics', 'fc' ),	'priority' => 50,		'capability' => 'edit_theme_options',
	) );
	
	// Create Settings
	$wp_customize->add_setting( 'fc_primary_contact', array('default' => '') );
	$wp_customize->add_setting( 'fc_phone_number', array('default' => '') );
	$wp_customize->add_setting( 'fc_fax_number', array('default' => '') );
	$wp_customize->add_setting( 'fc_email_address', array('default' => '') );
	$wp_customize->add_setting( 'fc_mailing_address', array('default' => '') );
	$wp_customize->add_setting( 'fc_city', array('default' => '') );
	$wp_customize->add_setting( 'fc_state', array('default' => '') );
	$wp_customize->add_setting( 'fc_zip', array('default' => '') );
	$wp_customize->add_setting( 'fc_copyright', array( 'default' => 'Website development by <a href="http://FlagstaffConnection.com" target="_blank">FlagstaffConnection.com</a> &bull; Website design by <a href="http://WebDesigner.com" target="_blank">WebDesigner.com</a> &copy; 2015 All Rights Reserved.' ) );
	 
	$wp_customize->add_setting( 'fc_default_layout', array('default' => 'template_c') );
	$wp_customize->add_setting( 'fc_header_height', array('default' => '100px') );
	$wp_customize->add_setting( 'fc_header_width', array('default' => '100%') );
	$wp_customize->add_setting( 'fc_hide_title', array('default' => 'false') );
	$wp_customize->add_setting( 'fc_hide_subtitle', array('default' => 'false') );
	$wp_customize->add_setting( 'fc_hide_logo', array('default' => 'false') );
	$wp_customize->add_setting( 'fc_logo_height', array('default' => '50px') );
	$wp_customize->add_setting( 'fc_logo_width', array('default' => '50px') );
	
	$wp_customize->add_setting( 'fc_cta_buttons_position', array( 'default' => '' ) );
	$wp_customize->add_setting( 'fc_cta_banner_show', array( 'default' => 'true' ) );
	$wp_customize->add_setting( 'fc_cta_banner', array( 'default' => '' ) );
	
	$wp_customize->add_setting( 'fc_enable_minor_core_updates', array( 'default' => 'true' ) );
	$wp_customize->add_setting( 'fc_enable_major_core_updates', array( 'default' => 'true' ) );
	$wp_customize->add_setting( 'fc_enable_plugin_updates', array( 'default' => 'true' ) );
	$wp_customize->add_setting( 'fc_enable_theme_updates', array( 'default' => 'true' ) );
	$wp_customize->add_setting( 'fc_disable_all_updates', array( 'default' => 'false' ) );
	$wp_customize->add_setting( 'fc_disable_update_email', array( 'default' => 'false' ) );
	$wp_customize->add_setting( 'fc_disable_update_nag', array( 'default' => 'false' ) );
	$wp_customize->add_setting( 'fc_disable_admin_bar', array( 'default' => 'false' ) );
	$wp_customize->add_setting( 'fc_set_404_page', array( 'default' => 'false' ) );
	
	$wp_customize->add_setting( 'fc_custom_css', array( 'default' => '' ) );
	$wp_customize->add_setting( 'fc_custom_js', array( 'default' => '' ) );
	$wp_customize->add_setting( 'fc_custom_ga', array( 'default' => '' ) );
	
	// Create Controls
	// --- site information ---
	$wp_customize->add_control( 'fc_primary_contact_control', array(
		'settings'	=> 'fc_primary_contact', 				'label'	=> __( 'Primary Contact', 'fc' ),
		'section' 	=> 'title_tagline', 						'type'	=> 'text', 				'priority' => '20',
	) );
	
	$wp_customize->add_control( 'fc_phone_number_control', array(
		'settings'	=> 'fc_phone_number', 					'label'	=> __( 'Phone Number', 'fc' ),
		'section' 	=> 'title_tagline', 						'type'	=> 'text', 				'priority' => '20',
	) );
	
	$wp_customize->add_control( 'fc_fax_number_control', array(
		'settings'	=> 'fc_fax_number', 						'label'	=> __( 'Fax Number', 'fc' ),
		'section' 	=> 'title_tagline', 						'type'	=> 'text', 				'priority' => '20',
	) );
	
	$wp_customize->add_control( 'fc_email_address_control', array(
		'settings'	=> 'fc_email_address', 					'label'	=> __( 'Email Address', 'fc' ),
		'section' 	=> 'title_tagline', 						'type'	=> 'text', 				'priority' => '20',
	) );
	
	$wp_customize->add_control( 'fc_mailing_address_control', array(
		'settings'	=> 'fc_mailing_address', 				'label'	=> __( 'Mailing Address', 'fc' ),
		'section' 	=> 'title_tagline', 						'type'	=> 'text', 				'priority' => '20',
	) );
	
	$wp_customize->add_control( 'fc_city_control', array(
		'settings'	=> 'fc_city', 									'label'	=> __( 'City', 'fc' ),
		'section' 	=> 'title_tagline', 						'type'	=> 'text', 				'priority' => '20',
	) );
	
	$wp_customize->add_control( 'fc_state_control', array(
		'settings'	=> 'fc_state', 									'label'	=> __( 'State', 'fc' ),
		'section' 	=> 'title_tagline', 						'type'	=> 'text', 				'priority' => '20',
	) );
	
	$wp_customize->add_control( 'fc_zip_control', array(
		'settings'	=> 'fc_zip', 										'label'	=> __( 'Zip', 'fc' ),
		'section' 	=> 'title_tagline', 						'type'	=> 'text', 				'priority' => '20',
	) );
	
	// --- design and layout ---
	$wp_customize->add_control( 'fc_default_layout', array(
		'settings' => 'fc_default_layout',					'label' => __( 'Default Layout', 'fc' ),
		'section' => 'fc_theme_design_layout',			'type' => 'radio',				'priority' => '10',
		'choices' => array(
			'template_c' => '',
			'template_cs' => '',
			'template_sc' => '',
			'template_scs' => '',
		),
	) );
	
	$wp_customize->add_control( 'fc_header_height_control', array(
		'settings'	=> 'fc_header_height',					'label'	=> __( 'Header Height', 'fc' ),
		'section' 	=> 'fc_theme_design_layout',		'type'	=> 'text',				'priority' => '10',
	) );
		
	$wp_customize->add_control( 'fc_header_width_control', array(
		'settings'	=> 'fc_header_width', 						'label'	=> __( 'Header Width', 'fc' ),
		'section' 	=> 'fc_theme_design_layout', 		'type'	=> 'text', 				'priority' => '10',
	) );
	
	$wp_customize->add_control( 'fc_hide_title_control', array(
		'settings' => 'fc_hide_title',							'label' => __( 'Hide the page title', 'fc' ),
		'section' => 'fc_theme_design_layout',			'type' => 'checkbox',			'priority' => '10',
	) );
	
	$wp_customize->add_control( 'fc_hide_subtitle_control', array(
		'settings' => 'fc_hide_subtitle',						'label' => __( 'Hide the page sub title', 'fc' ),
		'section' => 'fc_theme_design_layout',			'type' => 'checkbox',			'priority' => '10',
	) );
	
	$wp_customize->add_control( 'fc_hide_logo_control', array(
		'settings' 	=> 'fc_hide_logo',							'label' => __( 'Hide the header logo', 'fc' ),
		'section' 	=> 'fc_theme_design_layout',		'type' 	=> 'checkbox',		'priority' => '10',
	) );
	
	$wp_customize->add_control( 'fc_logo_height_control', array(
		'settings'	=> 'fc_logo_height',						'label'	=> __( 'Logo Height', 'fc' ),
		'section' 	=> 'fc_theme_design_layout',		'type'	=> 'text',				'priority' => '45',
	) );
		
	$wp_customize->add_control( 'fc_logo_width_control', array(
		'settings'	=> 'fc_logo_width', 						'label'	=> __( 'Logo Width', 'fc' ),
		'section' 	=> 'fc_theme_design_layout', 		'type'	=> 'text', 				'priority' => '45',
	) );
	
	// Tweaks and Settings	
	$wp_customize->add_control( 'fc_disable_all_updates_control', array(
		'settings' 	=> 'fc_disable_all_updates',		'label' => __( 'Disable all Automatic Updating of the wordpress core, plugins, and themes', 'fc' ),
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'checkbox',		'priority' => '10',
	) );
	
	$wp_customize->add_control( 'fc_enable_minor_core_updates_control', array(
		'settings' 	=> 'fc_enable_minor_core_updates',	'label' => __( 'Enable automatic minor wordpress core updates (eg. 4.x.x)', 'fc' ),
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'checkbox',		'priority' => '10',		'active_callback' => 'fc_disable_all_updates_is_checked',
	) );
	
	$wp_customize->add_control( 'fc_enable_major_core_updates_control', array(
		'settings' 	=> 'fc_enable_major_core_updates',	'label' => __( 'Enable automatic major wordpress core updates (eg. 4.x)', 'fc' ),
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'checkbox',		'priority' => '10',		'active_callback' => 'fc_disable_all_updates_is_checked',
	) );
	
	$wp_customize->add_control( 'fc_enable_plugin_updates_control', array(
		'settings' 	=> 'fc_enable_plugin_updates',	'label' => __( 'Enable automatic plugin updates', 'fc' ),
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'checkbox',		'priority' => '10',		'active_callback' => 'fc_disable_all_updates_is_checked',
	) );
	
	$wp_customize->add_control( 'fc_enable_theme_updates_control', array(
		'settings' 	=> 'fc_enable_theme_updates',		'label' => __( 'Enable automatic theme updates', 'fc' ),
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'checkbox',		'priority' => '10',		'active_callback' => 'fc_disable_all_updates_is_checked',
	) );
	
	$wp_customize->add_control( 'fc_disable_update_email_control', array(
		'settings' 	=> 'fc_disable_update_email',		'label' => __( 'Disable the email that gets sent out when updates are completed.', 'fc' ),
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'checkbox',		'priority' => '10',
	) );
	
	$wp_customize->add_control( 'fc_disable_update_nag_control', array(
		'settings' 	=> 'fc_disable_update_nag',			'label' => __( 'Disable the update nag message that appears on the top of the wordpress admin console.', 'fc' ),
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'checkbox',		'priority' => '10',
	) );
	
	$wp_customize->add_control( 'fc_disable_admin_bar_control', array(
		'settings' 	=> 'fc_disable_admin_bar',			'label' => __( 'Disable admin bar for all users except admin.', 'fc' ),
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'checkbox',		'priority' => '10',
	) );
	
	$wp_customize->add_control( 'fc_set_404_page_control', array(
		'settings' 	=> 'fc_set_404_page',						'label'   => 'Designate a custom 404 page',
		'section' 	=> 'fc_theme_tweaks',						'type' 	=> 'dropdown-pages',		'priority' => '10',
	) );
	
	// --- design and layout ---
	$wp_customize->add_control( 'fc_cta_buttons_position', array(
		'settings' => 'fc_cta_buttons_position',		'label' => __( 'Where to show the CTA buttons', 'fc' ),
		'section' => 'fc_theme_cta_options',				'type' => 'select',				'priority' => '1',
		'choices' => array(
			'' => 'Do Not Display',
			'left' => 'Left Sidebar',
			'right' => 'Right Sidebar',
		),
	) );
	
	$wp_customize->add_control( 'fc_cta_banner_show', array(
		'settings' 	=> 'fc_cta_banner_show',				'label' => __( 'Show the CTA Banner on all pages by default', 'fc' ),
		'section' 	=> 'fc_theme_cta_options',			'type' 	=> 'checkbox',		'priority' => '10',
	) );
	
	// Footer Content Areas
	$wp_customize->add_control( 'fc_cta_banner', array(
	  'settings' 	=> 'fc_cta_banner',							'label'   => __( 'Call to action banner', 'fc' ),
	  'section' 	=> 'fc_theme_cta_options',			'type'	=> 'textarea',		'priority' => '1',
	) );
	
	$wp_customize->add_control( new Text_Editor_Custom_Control( $wp_customize, 'fc_copyright', array(
	 'label'   => __( 'Copyright Message', 'fc' ),
	 'section' => 'fc_theme_footer_copy',
	 'settings'   => 'fc_copyright',
	 'priority' => 10
	) ) );
	
	$wp_customize->add_control( new Text_Editor_ONLY_Custom_Control( $wp_customize, 'fc_custom_css', array(
	 'label'   => __( 'Custom CSS', 'fc' ),
	 'section' => 'fc_theme_custom_code',
	 'settings'   => 'fc_custom_css',
	 'priority' => 100
	) ) );
	
	$wp_customize->add_control( new Text_Editor_ONLY_Custom_Control( $wp_customize, 'fc_custom_js', array(
	 'label'   => __( 'Custom Javascript', 'fc' ),
	 'section' => 'fc_theme_custom_code',
	 'settings'   => 'fc_custom_js',
	 'priority' => 100
	) ) );
	
	$wp_customize->add_control( new Text_Editor_ONLY_Custom_Control( $wp_customize, 'fc_custom_ga', array(
	 'label'   => __( 'Google Analytics Code', 'fc' ),
	 'section' => 'fc_theme_custom_code',
	 'settings'   => 'fc_custom_ga',
	 'priority' => 100
	) ) );
}
add_action( 'customize_register' , 'fc_theme_options' );

// ACTIVE CALLBACKS FOR CONTROLS ABOVE
function fc_disable_all_updates_is_checked(){
	$fc_disable_all_updates = get_theme_mod( 'fc_disable_all_updates');
	if( $fc_disable_all_updates ) { return false; }
	return true;
}

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
// Enqueue Javascript postMessage handlers for the Customizer.
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function fc_customize_preview_js() {
	wp_enqueue_script( 'fc-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20141120', true );
}
add_action( 'customize_preview_init', 'fc_customize_preview_js' );
?>