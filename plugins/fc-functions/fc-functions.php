<?php
/*
Plugin Name: FlagConn Functionality Plugin
Plugin URL: http://flagstaffconnection.com/
Description: A single repository for all site functionality
Version: 1.0
Author: Jesse Waitz
Author URI: http://flagstaffconnection.com
Contributors:
*/

require_once 'fc-func-widgets.php';
require_once 'fc-func-general.php';
require_once 'fc-func-site-options.php';
require_once 'fc-func-accounts.php'; ?>