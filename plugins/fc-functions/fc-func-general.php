<?php

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% MY DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
define( 'PLUGURL',	plugin_dir_url( __FILE__ ).'/' );	//echo "<pre>"; print_r(PLUGURL); echo "</pre>";
define( 'PLUGDIR',	dirname( __FILE__ ).'/' );				//echo "<pre>"; print_r(PLUGDIR); echo "</pre>";

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% ENQUEUE ICON PICKER IN ADMIN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function fc_icon_picker_scripts() {
	wp_enqueue_style( 'fontawesome', CSSURL.'font-awesome.css', null, null );
	wp_enqueue_style( 'icon-picker', PLUGURL . 'css/icon-picker.css', array( 'fontawesome' ), '1.0' );
	wp_enqueue_script( 'icon-picker', PLUGURL . 'js/icon-picker.js', array( 'jquery' ), '1.0', true );
}
add_action( 'admin_enqueue_scripts', 'fc_icon_picker_scripts' );

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% ADD BODY CLASSES FOR BROWSERS AND PHONES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function myfunction_browser_body_class($classes) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
 
    if($is_lynx) $classes[] = 'lynx';
    elseif($is_gecko) $classes[] = 'gecko';
    elseif($is_opera) $classes[] = 'opera';
    elseif($is_NS4) $classes[] = 'ns4';
    elseif($is_safari) $classes[] = 'safari';
    elseif($is_chrome) $classes[] = 'chrome';
    elseif($is_IE) $classes[] = 'ie';
    else $classes[] = 'unknown_browser';
 
    if($is_iphone) $classes[] = 'iphone';
    return $classes;
}
add_filter('body_class','myfunction_browser_body_class');

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% CUSTOMIZE WHAT USER META FIELDS ARE VISIBLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function wd_edit_fields( $contactmethods ) {
	unset($contactmethods['aim']);
	unset($contactmethods['jabber']);
	unset($contactmethods['yim']);
	unset($contactmethods['googleplus']);
	unset($contactmethods['twitter']);
	unset($contactmethods['facebook']);
	return $contactmethods;
}
add_filter('user_contactmethods','wd_edit_fields',11);

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% STRING HANDLING FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function string_limit_words($string, $word_limit) {
   $words = explode(' ', $string);
   $word_count = count($words);
   $new_str =  implode(' ', array_slice($words, 0, $word_limit));
   if ($word_count > $word_limit){ $new_str = $new_str."..."; }
   return $new_str;
}
function echoThis($temp_lbl, $temp_val) {
	echo "<b>".$temp_lbl."</b>: ".$temp_val."<br />";
}
function dbEncode($temp_str){       
	if(strpos(str_replace("\'",""," $temp_str"),"'")!=false) {
		$temp_str = trim( preg_replace( '/\s+/', ' ', $temp_str ) );
		return addslashes($temp_str);
	} else {
		$temp_str = trim( preg_replace( '/\s+/', ' ', $temp_str ) );
		return $temp_str;
	}
}
function dbDecode($temp_str) {
	$temp_str = stripslashes($temp_str);
	$temp_str = str_replace('"', '&quot;', $temp_str);
	$temp_str = $temp_str;
	return $temp_str;
}
function getLongDate($tmp_date) {
	$tmp_date = date("Y-m-d", strtotime($tmp_date));
	return $tmp_date;
}
function getLongTime($tmp_date) {
	$tmp_date = date("H:i:s", strtotime($tmp_date));
	return $tmp_date;
}
function getPrice($temp_str) {
	$temp_str = str_replace("\$",""," $temp_str");
	$temp_str = "\$".number_format($temp_str, 2, '.', ',');
	return $temp_str;
}
function stripPrice($temp_str) {
	$temp_str = str_replace("\$","","$temp_str");
	$temp_str = str_replace(",","","$temp_str");
	$temp_str = number_format($temp_str, 2, '.', '');
	return $temp_str;
}
function selectPrice($size_id) {
	if (is_logged_in() && is_pro()) {
		$temp_price = getNameByID("dealer_price", "size_id", $size_id, "sizes");
	} elseif (getNameByID("status", "size_id", $size_id, "sizes") == "on_sale") {
		$temp_price = getNameByID("sale_price", "size_id", $size_id, "sizes");
	} else {
		$temp_price = getNameByID("our_price", "size_id", $size_id, "sizes");
	}
	return $temp_price;
}
function getTaxRate($temp_str){       
	$temp_str = str_replace("\%",""," $temp_str");
	$temp_str = $temp_str/100;
	return $temp_str;
}
function getWebsiteURL($temp_string) {
	if (substr($temp_string, 0, 7) == "http://") {
		$temp_str = $temp_string;
	} else {
		$temp_str = "http://".$temp_string;
	}
	return $temp_str;
}
function notBlank($prefix, $temp_str, $suffix) {
	if ($temp_str != "") { $final_str = $prefix.$temp_str.$suffix; }
	return $final_str;
}
function notBlankArr($prefix, $temp_str, $suffix) {
	if (is_array($temp_str)) { $final_str = $prefix.implode(', ', $temp_str).$suffix; }
	return $final_str;
}
function notBlankURL($prefix, $temp_str, $suffix) {
	$temp_str = autolink($temp_str, array("target"=>"_blank"));
	if ($temp_str != "") { $final_str = $prefix.$temp_str.$suffix; }
	return $final_str;
}
function notBlank2Items($prefix, $temp_str1, $separator, $temp_str2, $suffix) {
	if ($temp_str1 != "" && $temp_str2 != "") { $final_str = $prefix.$temp_str1.$separator.$temp_str2.$suffix; }
	if ($temp_str1 == "" && $temp_str2 != "") { $final_str = $prefix.$temp_str2.$suffix; }
	if ($temp_str1 != "" && $temp_str2 == "") { $final_str = $prefix.$temp_str1.$suffix; }
	return $final_str;
}
function autolink($str, $attributes=array()) {
	$attrs = '';
	foreach ($attributes as $attribute => $value) {
		$attrs .= " {$attribute}=\"{$value}\"";
	}
	$str = ' ' . $str;
	$str = preg_replace(
		'`([^"=\'>])((http|https|ftp)://[^\s<]+[^\s<\.)])`i',
		'$1<a href="$2"'.$attrs.'>$2</a>',
		$str
	);
	$str = substr($str, 1);
	return $str;
}
function get_dynamic_sidebar($index = 1) {
	$sidebar_contents = "";
	ob_start();
	dynamic_sidebar($index);
	$sidebar_contents = ob_get_contents();
	ob_end_clean();
	return $sidebar_contents;
}

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% CONFIGURE THE AUTOMATIC UPDATE SETTINGS FOR WP CORE, THEMES, AND PLUGINS %%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

$enable_minor_core_updates = (get_theme_mod('fc_enable_minor_core_updates')==1) ? '__return_true' : '__return_false';	//echo "<pre>"; print_r($enable_minor_core_updates); echo "</pre>";
$enable_major_core_updates = (get_theme_mod('fc_enable_major_core_updates')==1) ? '__return_true' : '__return_false';	//echo "<pre>"; print_r($enable_major_core_updates); echo "</pre>";
$enable_plugin_updates = (get_theme_mod('fc_enable_plugin_updates')==1) ? '__return_true' : '__return_false';					//echo "<pre>"; print_r($enable_plugin_updates); echo "</pre>";
$enable_theme_updates = (get_theme_mod('fc_enable_theme_updates')==1) ? '__return_true' : '__return_false';						//echo "<pre>"; print_r($enable_theme_updates); echo "</pre>";
$disable_all_updates = (get_theme_mod('fc_disable_all_updates')==1) ? '__return_true' : '__return_false'; 						//echo "<pre>"; print_r($disable_all_updates); echo "</pre>";
$disable_update_email = (get_theme_mod('fc_disable_update_email')==1) ? '__return_true' : '__return_false';						//echo "<pre>"; print_r($disable_update_email); echo "</pre>";
$disable_update_nag = (get_theme_mod('fc_disable_update_nag')==1) ? '1' : '0';																				//echo "<pre>"; print_r($disable_update_nag); echo "</pre>";

add_filter( 'allow_minor_auto_core_updates', $enable_minor_core_updates );		// disable minor updates
add_filter( 'allow_major_auto_core_updates', $enable_major_core_updates );		// disable major updates
add_filter( 'auto_update_plugin', $enable_plugin_updates );										// enable automatic updates for all plugins
add_filter( 'auto_update_theme', $enable_theme_updates );											// enable automatic updates for all themes
add_filter( 'automatic_updater_disabled', $disable_all_updates );							// disable all automatic updates
add_filter( 'auto_core_update_send_email', $disable_update_email );						// disable update emails
if ($disable_update_nag) { add_action( 'admin_menu', function() { remove_action( 'admin_notices', 'update_nag', 3 ); } ); } // disable Update Nag from appearing at the top of all pages.

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% ADD GENERAL SHORTCODES TO THE SITE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
add_shortcode('site_url', function() { return get_option('home')."/"; });
add_shortcode('wpsearch', 'get_search_form');

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% ADD SHORTCODES FOR SITE'S GENERAL OPTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function fc12_page_title() { return get_bloginfo( 'name', 'display' ); }
add_shortcode('fc12_page_title', 'fc12_page_title');

function fc12_sub_title() { return get_bloginfo( 'description', 'display' ); }
add_shortcode('fc12_sub_title', 'fc12_sub_title');

function fc12_primary_contact() { return get_theme_mod('fc_primary_contact'); }
add_shortcode('fc12_primary_contact', 'fc12_primary_contact');

function fc12_phone_number() { return get_theme_mod('fc_phone_number'); }
add_shortcode('fc12_phone_number', 'fc12_phone_number');

function fc12_fax_number() { return get_theme_mod('fc_fax_number'); }
add_shortcode('fc12_fax_number', 'fc12_fax_number');

function fc12_email_address() { return get_theme_mod('fc_email_address'); }
add_shortcode('fc12_email_address', 'fc12_email_address');

function fc12_mailing_address() { return get_theme_mod('fc_mailing_address'); }
add_shortcode('fc12_mailing_address', 'fc12_mailing_address');

function fc12_city() { return get_theme_mod('fc_city'); }
add_shortcode('fc12_city', 'fc12_city');

function fc12_state() { return get_theme_mod('fc_state'); }
add_shortcode('fc12_state', 'fc12_state');

function fc12_zip() { return get_theme_mod('fc_zip'); }
add_shortcode('fc12_zip', 'fc12_zip');

function fc12_add_my_tc_button() {
	global $typenow;
	// check user permissions
	if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) { return; }
	// check if WYSIWYG is enabled
	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "fc12_add_tinymce_plugin");
		add_filter('mce_buttons', 'fc12_register_my_tc_button');
	}
}
function fc12_add_tinymce_plugin($plugin_array) {
	$plugin_array['fc12_tc_button'] = JSURL.'custom_tinymce.js'; // CHANGE THE BUTTON SCRIPT HERE
	return $plugin_array;
}
function fc12_register_my_tc_button($buttons) {
	array_push($buttons, "fc12_tc_button");
	return $buttons;
}
add_action('admin_head', 'fc12_add_my_tc_button');

//allow running shortcode in sidebar widgets
add_filter('widget_text', 'do_shortcode');
 
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% CHANGE MAXIMUM UPLOAD SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function WP_increase_upload_size( $bytes ) { return 262144000; }
add_filter( 'upload_size_limit', 'WP_increase_upload_size' );

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% DISABLE ADMIN BAR FOR ALL USER EXCEPT ADMIN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
$disable_admin_bar = get_theme_mod('fc_disable_admin_bar');
if ($disable_admin_bar) {
	function remove_admin_bar() { if (!current_user_can('administrator') && !is_admin()) { show_admin_bar(false); } }
	add_action('after_setup_theme', 'remove_admin_bar');
}
?>