<?php
/**
 * Template: Searchform.php
 *
 * @package WPFramework
 * @subpackage Template
 */
?>
<!--BEGIN .searchform-->
<form role="search" method="get" class="searchform" action="<?php bloginfo( 'url' ); ?>">
	<div><label class="screen-reader-text" for="s">Search for:</label>
	<input type="text" value="" name="s" class="s">
	<input type="submit" class="searchsubmit" value="&#xf002;">
	</div>
</form><!--END .searchform-->