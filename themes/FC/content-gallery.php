<?php
/**
* The template used for displaying page content in page.php
*
* @package WordPress
* @subpackage Twenty_Twelve
* @since Twenty Twelve 1.0
*/
$galleries = get_field('galleries');
$galleries = isset($galleries) ? $galleries : '';
?>

<?php if ($galleries) { $x = 0; ?>
	<?php foreach ($galleries as $item) { ?>
	<div class="gallery">
		<div class="gallery_title">
			<h3><?php echo $item['title']; ?> &nbsp; <span><a href="#none" class="show-hide">(Hide)</a></span></h3>
			<?php echo $item['description']; ?>
		</div>
		<div class="gallery_photos">
			<?php foreach ($item['gallery'] as $photo) { ?>
			<?php //echo "<pre>"; print_r($photo); echo "</pre>"; ?>
			<a rel="gallery<?php echo $x; ?>" id="photo_<?php echo $photo['id']; ?>" class="single" href="<?php echo $photo['url']; ?>" title="<?php echo $photo['title']; ?>"><img alt="<?php echo $photo['alt']; ?>" src="<?php echo $photo['sizes']['thumbnail']; ?>" /></a>
			<?php } ?>
		</div>
		<script language="JavaScript" type="text/javascript">
			jQuery(document).ready(function($) {
				$('.show-hide').toggle(function () {
					$(this).text('(Show)').parent().parent().parent().parent().find('.gallery_photos').slideUp();
				},function () {
					$(this).text('(Hide)').parent().parent().parent().parent().find('.gallery_photos').slideDown();
				});
				
				$("a[rel=gallery<?php echo $x; ?>]").fancybox({
					overlayColor	: "#000",
					transitionIn	: 'none',
					transitionOut	: 'none',
					titlePosition	: 'over',
					titleFormat		: function(title, currentArray, currentIndex, currentOpts) {
						return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
					}
				});
			}); 
		</script>
	</div>
	<?php $x++; } ?>
<?php } else { ?>
	<p>There are no photos in this gallery.</p>
<?php } ?>