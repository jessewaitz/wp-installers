<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

$hide_cta_banner = get_field('hide_cta_banner');
$cta_banner_show = get_theme_mod('fc_cta_banner_show');
$show_cta_banner = ($hide_cta_banner) ? '': $cta_banner_show;
$cta_banner = get_theme_mod('fc_cta_banner');

$copyright = get_theme_mod('fc_copyright');
$custom_js = get_theme_mod('fc_custom_js');
$custom_ga = get_theme_mod('fc_custom_ga');
?>
	</div><!-- #main .wrapper -->
	
	<?php if ( $show_cta_banner ) { ?>
	<div class="cta-banner">
		<?php echo $cta_banner; ?>
	</div><!-- .cta-banner -->
	<?php } ?>
	
	<footer id="colophon" role="contentinfo">
		<div class="site-footer">
			<?php if ( is_active_sidebar( 'footer' ) ) { dynamic_sidebar( 'footer' ); } ?>
			<div class="site-copyright">
				<?php echo $copyright; ?>
				<?php if ( has_nav_menu( 'utility-menu' ) ) { wp_nav_menu( array( 'theme_location' => 'utility-menu', 'container' => '' ) ); } ?>
			</div><!-- .site-info -->
		</div><!-- .site-footer -->
	</footer><!-- #colophon -->
	
</div><!-- #page -->

<?php wp_footer(); ?>
<?php if ($custom_js) { echo "<script>".$custom_js."</script>"; } ?>
<?php if ($custom_ga) { echo $custom_ga; } ?>
</body>
</html>