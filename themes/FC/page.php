<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

$custom_sidebars = get_field('custom_sidebars');
$cta_buttons_position = get_theme_mod( 'fc_cta_buttons_position' );
$cta_buttons = get_dynamic_sidebar( 'cta' );

if ($custom_sidebars) {
	$page_layout = get_field('page_layout');
	$sidebar_right = get_field('sidebar_right');
	$sidebar_left = get_field('sidebar_left');
} else {
	$page_layout = get_theme_mod( 'fc_default_layout' );
	$sidebar_left = get_dynamic_sidebar( 'sidebar2' );
	$sidebar_right = get_dynamic_sidebar( 'sidebar3' );
}

get_header(); ?>

	<?php if(is_front_page()) { ?>
	<div class="entry-other">
		<?php get_template_part( 'content', 'other' ); ?>
	</div><!-- .entry-other -->
	<?php } ?>

	<?php if (in_array($page_layout, array("template_sc", "template_scs"))) { ?>
	<div class="sidebar-left widget-area">
		<?php if ($cta_buttons_position == 'left') { echo $cta_buttons; } ?>
		<?php if ($sidebar_left) { echo $sidebar_left; } ?>
	</div><!-- .sidebar-left -->
	<?php } ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<?php if (in_array($page_layout, array("template_cs", "template_scs"))) { ?>
	<div class="sidebar-right widget-area">
		<?php if ($cta_buttons_position == 'right') { echo $cta_buttons; } ?>
		<?php if ($sidebar_right) { echo $sidebar_right; } ?>
	</div><!-- .sidebar-regular -->
	<?php } ?>

<?php get_footer(); ?>