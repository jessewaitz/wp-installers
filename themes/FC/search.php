<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

$custom_sidebars = get_field('custom_sidebars');
$page_layout = ($custom_sidebars) ? get_field('page_layout') : get_theme_mod( 'fc_default_layout' );

get_header(); ?>
	
	<?php if (in_array($page_layout, array("template_sc", "template_scs"))) { ?>
	<div class="sidebar-left">
		<div class="sidebar_box clear"><?php get_sidebar(); ?></div>
	</div><!-- .sidebar-left -->
	<?php } ?>

	<section id="primary" class="site-content">
		<div id="content" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentytwelve' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header>

			<?php fc_content_nav( 'nav-above' ); ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php fc_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<article id="post-0" class="post no-results not-found">
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentytwelve' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-0 -->

		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->

	<?php if (in_array($page_layout, array("template_cs", "template_scs"))) { ?>
	<div class="sidebar-right">
		<div class="sidebar_box clear"><?php get_sidebar(); ?></div>
	</div><!-- .sidebar-regular -->
	<?php } ?>

<?php get_footer(); ?>