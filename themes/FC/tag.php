<?php
/**
 * The template for displaying Tag pages.
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

$custom_sidebars = get_field('custom_sidebars');
$page_layout = ($custom_sidebars) ? get_field('page_layout') : get_theme_mod( 'fc_default_layout' );

get_header(); ?>
	
	<?php if (in_array($page_layout, array("template_sc", "template_scs"))) { ?>
	<div class="sidebar-left">
		<div class="sidebar_box clear"><?php get_sidebar(); ?></div>
	</div><!-- .sidebar-left -->
	<?php } ?>

	<section id="primary" class="site-content">
		<div id="content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( 'Tag Archives: %s', 'twentytwelve' ), '<span>' . single_tag_title( '', false ) . '</span>' ); ?></h1>

			<?php if ( tag_description() ) : // Show an optional tag description ?>
				<div class="archive-meta"><?php echo tag_description(); ?></div>
			<?php endif; ?>
			</header><!-- .archive-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/* Include the post format-specific template for the content. If you want to
				 * this in a child theme then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

			endwhile;

			fc_content_nav( 'nav-below' );
			?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->

	<?php if (in_array($page_layout, array("template_cs", "template_scs"))) { ?>
	<div class="sidebar-right">
		<div class="sidebar_box clear"><?php get_sidebar(); ?></div>
	</div><!-- .sidebar-regular -->
	<?php } ?>

<?php get_footer(); ?>