<?php
$select_custom_404_page = get_theme_mod('fc_set_404_page');
$custom_404_page_link = get_post($select_custom_404_page); //echo "<pre>"; print_r($custom_404_page_link); echo "</pre>";

if ($select_custom_404_page) {		
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: ".$custom_404_page_link->guid);
	exit();
}
?>
<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

$custom_sidebars = get_field('custom_sidebars');
$page_layout = ($custom_sidebars) ? get_field('page_layout') : get_theme_mod( 'fc_default_layout' );

get_header(); ?>
	
	<?php if (in_array($page_layout, array("template_sc", "template_scs"))) { ?>
	<div class="sidebar-left">
		<div class="sidebar_box clear"><?php get_sidebar(); ?></div>
	</div><!-- .sidebar-left -->
	<?php } ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<article id="post-0" class="post error404 no-results not-found">
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'File or Page Not Found', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentytwelve' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-0 -->

		</div><!-- #content -->
	</div><!-- #primary -->

	<?php if (in_array($page_layout, array("template_cs", "template_scs"))) { ?>
	<div class="sidebar-right">
		<div class="sidebar_box clear"><?php get_sidebar(); ?></div>
	</div><!-- .sidebar-regular -->
	<?php } ?>

<?php get_footer(); ?>