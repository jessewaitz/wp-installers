<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

$site_title = get_bloginfo( 'name', 'display' );
$site_description = get_bloginfo( 'description', 'display' );
$hide_page_title = get_theme_mod( 'fc_hide_title' );
$hide_sub_title = get_theme_mod( 'fc_hide_subtitle' );
$custom_css = get_theme_mod('fc_custom_css');

$hide_logo = get_theme_mod( 'fc_hide_logo' );
$logo_url = get_site_icon_url();
$logo_height = get_theme_mod('fc_logo_height');
$logo_width = get_theme_mod('fc_logo_width');

$header_minimum_height = get_theme_mod('fc_header_height');
$header_maximum_width = get_theme_mod('fc_header_width');

$banner_selector = get_field('banner_selector');
$slider_uploader = get_field('slider_uploader');
$slider_effect = get_field('slider_effect');
$pause_time = get_field('pause_time');
$animation_speed = get_field('animation_speed');

$heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div';
$sub_heading_tag = ( is_home() || is_front_page() ) ? 'h2' : 'div';
$page_layout = (get_field('custom_sidebars')) ? get_field('page_layout') : get_theme_mod( 'fc_default_layout' );

$total_widgets = wp_get_sidebars_widgets(); 
$footer_boxes = count($total_widgets[ 'footer' ]);
if ($footer_boxes) {
	$footer_box_count = $footer_boxes;
	$footer_box_width = (100/$footer_box_count) - (2 - 2/$footer_box_count);
}
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->

<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
<script>( function( $ ) { HOMEURL = "<?php echo dirname( get_bloginfo('stylesheet_url')); ?>"; } )( jQuery );</script>
<style type="text/css" media="screen">
	<?php if ($hide_page_title) { ?>.site-title a span { height: 0; width: 0; text-indent: -9999em; }<?php } ?>
	<?php if ($hide_sub_title) { ?>.site-description { height: 0; width: 0; text-indent: -9999em; }<?php } ?>
	.site-header .site-logo { min-height: <?php echo $header_minimum_height; ?>; }
	.site-header .site-title a img { max-height: <?php echo $logo_height; ?>; max-width: <?php echo $logo_width; ?>; line-height: 1; vertical-align: middle; }
	.menu-spacer { min-height: <?php echo $header_minimum_height; ?>; max-width: <?php echo $header_maximum_width; ?>; }
	.menu-spacer img { height: <?php echo $logo_height; ?>; width: <?php echo $logo_width; ?>; padding: 0; line-height: 1; }
	.main-navigation .nav-menu { top: <?php echo $header_minimum_height; ?>; }
	@media screen and (min-width: <?php echo TABLET; ?>) { .menu-spacer { margin-top: 45px; } }
	@media screen and (min-width: <?php echo TABLET; ?>) { .site-footer .widget { width: <?php echo $footer_box_width; ?>%; } }
	@media screen and (min-width: <?php echo DESKTOP; ?>) { .main-navigation .nav-menu { top: 0; } }
	<?php if ($custom_css) { echo $custom_css; } ?>
</style>
</head>
			
<body <?php body_class($page_layout); ?>>
<div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-logo">
			<<?php echo $heading_tag; ?> class="site-title"><a href="<?php echo HOMEURL; ?>" title="<?php echo esc_attr( $site_title ); ?>" rel="home"><?php if (!$hide_logo) { if ( function_exists( 'the_custom_logo' ) ) { the_custom_logo(); } } ?><span><?php echo $site_title; ?></span></a></<?php echo $heading_tag; ?>>
			<<?php echo $sub_heading_tag; ?> class="site-description"><?php echo $site_description; ?></<?php echo $sub_heading_tag; ?>>
		</div>
		
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<h3 class="menu-toggle fa fa-bars"><span><?php _e( 'Menu', 'twentytwelve' ); ?></span></h3>
			<?php if ( has_nav_menu( 'primary' ) ) { wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); } ?>
			<h3 class="my-account-toggle fa fa-user"><span><?php _e( 'My Account', 'twentytwelve' ); ?></span></h3>
			<div class="menu-my-account">
				<ul class="nav-menu">
					<?php if (is_user_logged_in()) { ?>
					<li><a href="<?php echo HOMEURL; ?>support/my-profile/">My Profile</a></li>
					<li><a href="<?php echo HOMEURL; ?>support/">View My Tickets</a></li>
					<li><a href="<?php echo HOMEURL; ?>support/add-new-ticket/">Add New Ticket</a></li>
					<li><a href="<?php echo wp_logout_url( home_url().$_SERVER['REQUEST_URI'] ); ?>">Logout</a></li>
					<?php } else { ?>
					<li><a href="<?php echo HOMEURL; ?>support/my-profile/">Login</a></li>
					<li><a href="<?php echo HOMEURL; ?>support/my-profile/">Register</a></li>
					<?php } ?>
				</ul>
			</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	
	<div class="menu-lightbox"></div>
	<div class="menu-spacer"><img src="<?php echo $logo_url; ?>" class="img-spacer" alt="Menu Spacer" height="0" width="0"></div>
	
	<?php if ($banner_selector) { ?>
	<div class="slideshow-photos" class="">
		<?php if($slider_uploader) { $i = 1;
		foreach($slider_uploader as $row) {
			$photo = wp_get_attachment_image_src($row['slider'], 'main-slider');
			$caption = $row['caption'];
			$link = $row['link']; ?>
			<?php if ($link) { ?><a href="<?php echo $link; ?>"><?php } ?><img src="<?php echo $photo[0]; ?>" class="slideshow-photo" alt="<?php echo $caption; ?>" title="<?php echo $caption; ?>"><?php if ($link) { ?></a><?php } ?>
		<?php $i++; } } ?>
	</div>
	<script language="JavaScript" type="text/javascript">
		jQuery(document).ready(function($) {
			var x = 2;
			$(".slideshow-photos").nivoSlider({ 
				effect:"<?php echo $slider_effect; ?>",animSpeed:<?php echo $animation_speed; ?>,pauseTime:<?php echo $pause_time; ?>,directionNav:false,directionNavHide:false,controlNav:false,pauseOnHover:false,slices:1,captionOpacity:1,startSlide:0
			}); 
		}); 
	</script>
	<div class="slideshow-mask"></div>
	<?php } ?>
	
	<div id="main" class="wrapper">