<?php
/**
* The template used for displaying page content in page.php
*
* @package WordPress
* @subpackage Twenty_Twelve
* @since Twenty Twelve 1.0
*/
$content_selector = get_field('content_selector');
$content_selector = isset($content_selector) ? $content_selector : '';
?>

<?php if($content_selector) { $i = 1; ?>
	<?php foreach($content_selector as $row) { ?>
		<?php if ($row['acf_fc_layout'] == "text_content") { ?>
		<div class="text_content <?php echo sanitize_title($row['section_header']); ?>">
			<?php if ($row['section_header']) { ?><h3><span><?php echo $row['section_header']; ?></span></h3><?php } ?>
			<?php if ($row['section_sub_header']) { ?><h4><?php echo $row['section_sub_header']; ?></h4><?php } ?>
			<?php if ($row['content']) { ?><div><?php echo $row['content']; ?></div><?php } ?>
		</div>
		<?php } ?>
		<?php if ($row['acf_fc_layout'] == "cta_boxes") { ?>
		<div class="cta_boxes clear <?php echo $row['type']; ?> <?php echo sanitize_title($row['section_header']); ?>">
			<?php if ($row['section_header']) { ?><h3><span><?php echo $row['section_header']; ?></span></h3><?php } ?>
			<?php if ($row['section_sub_header']) { ?><h4><?php echo $row['section_sub_header']; ?></h4><?php } ?>
			<?php if ($row['cta_box']) { $blog_col_width = ((100/count($row['cta_box'])))-4/count($row['cta_box']); ?>
			<style type="text/css" media="screen">@media screen and (min-width: <?php echo TABLET; ?>) { .cta_boxes.<?php echo sanitize_title($row['section_header']); ?> li { width: <?php echo $blog_col_width; ?>%; } }</style>
			<ul>
				<?php foreach($row['cta_box'] as $box) {
				$icon_label = (isset($box['icon_label'])) ? $box['icon_label'] : ''; //echo "<pre>"; print_r($icon_label); echo "</pre>";
				$icon = (isset($box['icon'])) ? wp_get_attachment_image_src($box['icon'], 'medium') : ''; //echo "<pre>"; print_r($icon); echo "</pre>";
				$bg_color = (isset($box['bg_color'])) ? $box['bg_color'] : '';
				$bg_image = (isset($box['bg_image'])) ? wp_get_attachment_image_src($box['bg_image'], 'cta-boxes') : ''; //echo "<pre>"; print_r($bg_image); echo "</pre>";
				$link_label = (isset($box['link_label'])) ? $box['link_label'] : '';
				$link = (isset($box['link'])) ? $box['link'] : '';
				$text_block = (isset($box['text_block'])) ? $box['text_block'] : '';
				$text_area = (isset($box['text_area'])) ? $box['text_area'] : '';
				if ($row['type']=='text_areas'){ $icon = '';$text_block = ''; } else { $text_area = ''; } ?><li>
					<div class="image_box" <?php if ($bg_color) { ?>style="background: <?php echo $bg_color; ?>;"<?php } ?>>
						<?php if ($icon) { ?><div class="icon"><img src="<?php echo $icon[0]; ?>" alt="<?php echo $icon_label; ?>" title="<?php echo $text_block; ?>"></div><?php } ?>
						<?php if ($link) { ?><a href="<?php echo $link; ?>" class="cta_link"><?php } ?><?php if ($icon_label) { ?><div class="icon_label"><?php echo $icon_label; ?></div><?php } ?><?php if ($link) { ?></a><?php } ?>
						<?php if ($bg_image) { ?><div class="bg_image"><img src="<?php echo $bg_image[0]; ?>" alt="<?php echo $icon_label; ?>" title="<?php echo $text_block; ?>"></div><?php } ?>
						<?php if ($text_block) { ?><div class="text_block"><?php echo $text_block; ?></div><?php } ?>
						<?php if ($text_area) { ?><div class="text_block"><?php echo $text_area; ?></div><?php } ?>
					</div>
				</li><?php } ?>
			</ul>
			<?php } ?>
		</div>
		<?php } ?>
		<?php if ($row['acf_fc_layout'] == "blog_posts") { $post_obj = $row['blog_posts']; ?>
		<div class="blog_content <?php echo sanitize_title($row['section_header']); ?>">
			<?php if ($row['section_header']) { ?><h3><span><?php echo $row['section_header']; ?></span></h3><?php } ?>
			<?php if ($row['section_sub-header']) { ?><h4><?php echo $row['section_sub-header']; ?></h4><?php } ?>
			<?php if ($post_obj) { $blog_col_width = ((100/count($post_obj)))-4/count($post_obj); ?>
			<style type="text/css" media="screen">@media screen and (min-width: <?php echo TABLET; ?>) { .blog_content.<?php echo sanitize_title($row['section_header']); ?> li { width: <?php echo $blog_col_width; ?>%; } }</style>
			<ul>
			<?php foreach ($post_obj as $my_post) { //echo "<pre>"; print_r($my_post); echo "</pre>";
				$post_thumb = get_the_post_thumbnail($my_post['blog_post']->ID, "acf_boxes"); ?><li>
					<div class="blog_wrapper">
						<?php if ($post_thumb) { echo $post_thumb; } ?>
						<p><a href="<?php echo $my_post['blog_post']->guid; ?>"><strong><?php echo $my_post['blog_post']->post_title; ?></strong></a></p> 
						<?php echo wpautop(strip_tags(string_limit_words($my_post['blog_post']->post_content, 40))); ?>
						<p><a href="<?php echo $my_post['blog_post']->guid; ?>"><b>read more</b></a></p>
					</div>
				</li><?php } ?>
			</ul>
			<?php } ?>
		</div>
		<?php } ?>
		<?php if ($row['acf_fc_layout'] == "gallery") { $gallery = $row['gallery']; //echo "<pre>"; print_r($row); echo "</pre>"; ?>
		<div class="gallery_content <?php echo sanitize_title($row['section_header']); ?>">
			<?php if ($gallery) { ?>
				<div class="gallery">
					<div class="gallery_title">
						<h3><span><?php echo $row['title']; ?></span></h3>
						<?php echo $row['description']; ?>
					</div>
					<div class="gallery_photos">
						<?php foreach ($gallery as $photo) { ?>
						<?php //echo "<pre>"; print_r($photo); echo "</pre>"; ?>
						<a rel="gallery<?php echo $i; ?>" id="photo_<?php echo $photo['id']; ?>" class="single" href="<?php echo $photo['url']; ?>" title="<?php echo $photo['title']; ?>"><img alt="<?php echo $photo['alt']; ?>" src="<?php echo $photo['sizes']['thumbnail']; ?>" /></a>
						<?php } ?>
					</div>
					<script language="JavaScript" type="text/javascript">
						jQuery(document).ready(function($) {
							$("a[rel=gallery<?php echo $i; ?>]").fancybox({
								overlayColor	: "#000",
								transitionIn	: 'none',
								transitionOut	: 'none',
								titlePosition	: 'over',
								titleFormat		: function(title, currentArray, currentIndex, currentOpts) {
									return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
								}
							});
						}); 
					</script>
				</div>
				<?php } else { ?>
				<p>There are no photos in this gallery.</p>
				<?php } ?>
		</div>
		<?php } ?>
	<?php } ?>
<?php } ?>
