<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

$custom_sidebars = get_field('custom_sidebars');
$page_layout = ($custom_sidebars) ? get_field('page_layout') : get_theme_mod( 'fc_default_layout' );

get_header(); ?>
	
	<?php if (in_array($page_layout, array("template_sc", "template_scs"))) { ?>
	<div class="sidebar-left">
		<div class="sidebar_box clear"><?php get_sidebar(); ?></div>
	</div><!-- .sidebar-left -->
	<?php } ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

				<nav class="nav-single">
					<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentytwelve' ); ?></h3>
					<!-- <span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentytwelve' ) . '</span> %title' ); ?></span> -->
					<!-- <span class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentytwelve' ) . '</span>' ); ?></span> -->
				</nav><!-- .nav-single -->

				<?php comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<?php if (in_array($page_layout, array("template_cs", "template_scs"))) { ?>
	<div class="sidebar-right">
		<div class="sidebar_box clear"><?php get_sidebar(); ?></div>
	</div><!-- .sidebar-regular -->
	<?php } ?>
	
<?php get_footer(); ?>