<?php
/**
 * FC functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * @package WordPress
 * @subpackage FC
 * @since FC 1.0
 */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% ENQUEUE/DEQUEUE STYLESHEETS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
function remove_default_stylesheet() {
	wp_dequeue_style( 'fc-style' );
	wp_deregister_style( 'fc-style' );
	wp_enqueue_style( 'fc-main-style', THEMEURL.'style-main.css', null, null );
	wp_enqueue_style( 'fc-child-style', THEMEURL.'style.css', array('fc-main-style'), null );
}
add_action( 'wp_enqueue_scripts', 'remove_default_stylesheet', 20 );

function fc_customizer_live_preview() {
	wp_enqueue_script( 'fc-themecustomizer', JSURL.'theme-customizer.js', array( 'jquery' ), true );
}
add_action( 'customize_controls_enqueue_scripts', 'fc_customizer_live_preview' );

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%% put your custom functions here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

define( 'PHONE',  '320px' );
define( 'TABLET',  '720px' );
define( 'DESKTOP',  '960px' );
?>