(function() {
	tinymce.PluginManager.add('fc12_tc_button', function( editor, url ) {
		editor.addButton( 'fc12_tc_button', {
			title: 'My test button',
			type: 'menubutton',
			icon: 'icon dashicons-update',
			menu: [ {
				text: 'Insert Page Title Shortcode',
				value: '[fc12_page_title]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert Sub Title Shortcode',
				value: '[fc12_sub_title]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert Primary Contact Shortcode',
				value: '[fc12_primary_contact]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert Phone Shortcode',
				value: '[fc12_phone_number]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert Fax Shortcode',
				value: '[fc12_fax_number]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert Email Shortcode',
				value: '[fc12_email_address]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert Address Shortcode',
				value: '[fc12_mailing_address]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert City Shortcode',
				value: '[fc12_city]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert State Shortcode',
				value: '[fc12_state]',
				onclick: function() { editor.insertContent(this.value()); }
			}, {
				text: 'Insert Zip Shortcode',
				value: '[fc12_zip]',
				onclick: function() { editor.insertContent(this.value()); }
			} ]
		});
	});
})();