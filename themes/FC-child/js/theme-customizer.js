jQuery(document).ready(function() {
	/* Move our widgets into the widgets panel */
	wp.customize.bind('ready', function() { //console.log(wp.customize.section( 'nav_menu[3]' ));
		wp.customize.section( 'sidebar-widgets-footer' ).panel( 'fc_theme_footer' );
		wp.customize.section( 'sidebar-widgets-footer' ).priority( '2' );
		wp.customize.section( 'sidebar-widgets-cta' ).panel( 'fc_theme_cta' );
		wp.customize.section( 'sidebar-widgets-cta' ).priority( '2' );
		wp.customize.section( 'nav_menu[3]' ).panel( 'fc_theme_footer' );
		wp.customize.section( 'nav_menu[3]' ).priority( '100' );
	});
});

( function( $ ) {
	wp.customizerCtrlEditor = {

		init: function() {

			$(window).load(function(){

				$('textarea.wp-editor-area').each(function(){
					var tArea = $(this),
						id = tArea.attr('id'),
						input = $('input[data-customize-setting-link="'+ id +'"]'),
						editor = tinyMCE.get(id),
						setChange,
						content;

					if(editor){
						editor.onChange.add(function (ed, e) {
							ed.save();
							content = editor.getContent();
							clearTimeout(setChange);
							setChange = setTimeout(function(){ input.val(content).trigger('change'); },500);
						});
					}

					if(editor){
						editor.onChange.add(function (ed, e) {
							ed.save();
							content = editor.getContent();
							clearTimeout(setChange);
							setChange = setTimeout(function(){ input.val(content).trigger('change'); },500);
						});
					}

					tArea.css({ visibility: 'visible' }).on('keyup', function(){
						content = tArea.val();
						clearTimeout(setChange);
						setChange = setTimeout(function(){ input.val(content).trigger('change'); },500);
					});
				});
			});
		}

	};

	wp.customizerCtrlEditor.init();

} )( jQuery );