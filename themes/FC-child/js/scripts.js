/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */
 ( function( $ ) {
	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	/* %%%%% MEDIA QUERY CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	
	// media query event handler
	if (window.matchMedia != null) {
		var mq275 = window.matchMedia("screen and (min-width: 275px)");
		mq275.addListener(WidthChange);
		WidthChange(mq275);
		var mq400 = window.matchMedia("screen and (min-width: 400px)");
		mq400.addListener(WidthChange);
		WidthChange(mq400);
		var mq500 = window.matchMedia("screen and (min-width: 500px)");
		mq500.addListener(WidthChange);
		WidthChange(mq500);
		var mq600 = window.matchMedia("screen and (min-width: 600px)");
		mq600.addListener(WidthChange);
		WidthChange(mq600);
		var mq960 = window.matchMedia("screen and (min-width: 960px)");
		mq960.addListener(WidthChange);
		WidthChange(mq960);
	}
	// media query change
	function WidthChange(mq) {//console.log(mq.media);
		if (mq.matches == true && mq.media == "screen and (min-width: 275px)") { // window width is at least 275px
			//console.log(mq);
		} 
		if (mq.matches == true && mq.media == "screen and (min-width: 400px)") { // window width is at least 400px
			//console.log(mq);
		} 
		if (mq.matches == true && mq.media == "screen and (min-width: 500px)") { // window width is at least 500px
			//console.log(mq);
		} 
		if (mq.matches == true && mq.media == "screen and (min-width: 600px)") { // window width is at least 600px
			//console.log(mq);
			$(".entry-meta").unbind("click");
			$('.menu-toggle').removeClass("open_mini_menu");
			$('.menu-lightbox').hide();
			$('.main-navigation ul li a.right-arrow').html('<img src="'+HOMEURL+'/img/mobile_menu_plus.png" class="right" />');
			$('.main-navigation ul li a.right-arrow').next().next().slideUp();
		} 
		if (mq.matches == true && mq.media == "screen and (min-width: 960px)") { // window width is at least 960px
			//console.log(mq);
		}
	}
	
	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	/* %%%%% MOBILE MENU ARROWS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	
	/* handle the mobile menu arrows */
	$('.main-navigation ul').hide();
	
	$('.menu-toggle').toggle(function() { 
		$('.main-navigation > div.menu-my-account > ul').slideUp('normal'); 
		$('.main-navigation > div.menu-main-menu-container > ul').slideDown('normal'); 
		$('.menu-lightbox').show(); 
		return false; 
	},function() { 
		$('.main-navigation > div.menu-my-account > ul').slideUp('normal');
		$('.main-navigation > div.menu-main-menu-container > ul').slideUp('normal'); 
		$('.menu-lightbox').hide(); 
		return false; 
	});
	$('.my-account-toggle').toggle(function() { 
		$('.main-navigation > div.menu-main-menu-container > ul').slideUp('normal'); 
		$('.main-navigation > div.menu-my-account > ul').slideDown('normal'); 
		$('.menu-lightbox').show(); 
		return false; 
	},function() { 
		$('.main-navigation > div.menu-main-menu-container > ul').slideUp('normal'); 
		$('.main-navigation > div.menu-my-account > ul').slideUp('normal'); 
		$('.menu-lightbox').hide(); 
		return false; 
	});
	
	$('.main-navigation ul ul').hide();
	$('.main-navigation ul li a.right-arrow').live("click", function() { 
		if($(this).next().next().css("display") == "none"){
			if($(this).next().next().is('ul')) { 
				$(this).html('<img src="'+HOMEURL+'/img/mobile_menu_minus.png" class="down" />');
				$(this).next().next().slideDown('normal');
				return false; 
			}
		} else {
			if($(this).next().next().is('ul')) { 
				$(this).html('<img src="'+HOMEURL+'/img/mobile_menu_plus.png" class="right" />');
				$(this).next().next().slideUp('normal');	
				return false; 
			}
		}
	});
	
	/* setup the mobile menu arrows */
	function mobilemenuinit() {
		$('.main-navigation ul li a').each(function() {
			if($(this).next().is('ul')) {
				$(this).before('<a href="#none" class="right-arrow"><img src="'+HOMEURL+'/img/mobile_menu_plus.png" class="right" /></a>');
			}
		});
	}
	mobilemenuinit();
	
	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	/* %%%%% fixing the cf7 validation error message issue %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	
	$.fn.wpcf7NotValidTip = function(message) {
		return this.each(function() {
			var into = $(this);
			into.append('<span class="wpcf7-not-valid-tip">' + message + '</span>');
			$('span.wpcf7-not-valid-tip').mouseover(function() {
				$(this).fadeOut('fast');
			});
			into.find(':input').mouseover(function() {
				into.find('.wpcf7-not-valid-tip').not(':hidden').fadeOut('fast');
			});
			into.find(':input').focus(function() {
				into.find('.wpcf7-not-valid-tip').not(':hidden').fadeOut('fast');
			});
		});
	};
	
	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	/* %%%%% OTHER CUSTOM FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	// show/hide the more info boxes
	$(".more_link").mouseenter( function () { $(this).parent().find(".more_popup").show(); });
	$(".more_link").mouseleave( function () { $(this).parent().find(".more_popup").hide(); });
	
} )( jQuery );